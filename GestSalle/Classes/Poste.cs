﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Poste
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public static List<Poste> GetAll(int id_salle) => PosteDao.GetAll(id_salle);
    }
}
