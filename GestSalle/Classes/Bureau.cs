﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Bureau
    {
        public int? Id { get; set; }
        private Size size;
        public Size Size
        {
            get => size;
            set
            {
                size = value;
                Postes = new Poste[value.Width, value.Height];
            }
        }
        public int Width
        {
            get => Size.Width;
            set => Size = new Size(value, Size.Height);
        }
        public int Height
        {
            get => Size.Height;
            set => Size = new Size(Size.Width, value);
        }
        public Point Location { get; set; }
        public Orientation Orientation { get; set; }

        public Poste[,] Postes { get; private set; }
        public Salle Salle { get; set; }
        public Bureau() { }
        public Bureau(Size size)
        {
            Size = size;
        }
        public Bureau(int width, int height) : this(new Size(width, height)) { }

        public Bureau(Size size, Point location) : this(size)
        {
            Location = location;
        }
        public Bureau(int width, int height, Point location) : this(new Size(width, height), location) { }
        public Bureau(Size size, Point location, Orientation orientation) : this(size, location)
        {
            Orientation = orientation;
        }
        public Bureau(int width, int height, Point location, Orientation orientation) : this(new Size(width, height), location, orientation) { }

        public static List<Bureau> GetAll(Salle salle) => BureauDao.GetAll(salle);
    }
}
