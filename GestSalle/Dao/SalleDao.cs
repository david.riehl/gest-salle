﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class SalleDao
    {
        public static List<Salle> GetAll()
        {
            List<Salle> items = new List<Salle>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * FROM `salle`";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Salle
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }
    }
}
