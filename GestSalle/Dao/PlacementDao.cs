﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class PlacementDao
    {
        public static void Save(Groupe groupe, Salle salle, Dictionary<Poste, Employe> placements)
        {
            List<Employe> items = new List<Employe>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query;
            MySqlCommand command;
            int nbRows;

            query = "DELETE FROM `groupe_salle_poste` WHERE `id_groupe` = ?id_groupe AND `id_salle` = ?id_salle;";
            command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", groupe.Id));
            command.Parameters.Add(new MySqlParameter("id_salle", salle.Id));
            nbRows = command.ExecuteNonQuery();

            query = "INSERT INTO `groupe_salle_poste` " +
                    "(`id_groupe`, `id_salle`, `id_poste`, `id_employe`)" +
                    "VALUES ";

            for (int i = 0; i < placements.Count; i++)
            {
                query += $"(?id_groupe, ?id_salle, ?id_poste_{i}, ?id_employe_{i}),\n";
            }
            query = query.Substring(0, query.Length -2);

            command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", groupe.Id));
            command.Parameters.Add(new MySqlParameter("id_salle", salle.Id));

            List<Poste> postes = placements.Keys.ToList();
            for (int i = 0; i < placements.Count; i++)
            {
                Poste poste = postes[i];
                Employe employe = placements[poste];
                command.Parameters.Add(new MySqlParameter($"id_poste_{i}", poste.Id));
                command.Parameters.Add(new MySqlParameter($"id_employe_{i}", employe.Id));
            }
            nbRows = command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
