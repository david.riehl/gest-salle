﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class BureauDao
    {
        public static List<Bureau> GetAll(Salle salle)
        {
            List<Bureau> items = new List<Bureau>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * FROM `bureau` WHERE id_salle = ?id_salle;";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_salle", salle.Id));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                int width = (int)reader["width"];
                int height = (int)reader["height"];
                int x = (int)reader["x"];
                int y = (int)reader["y"];
                items.Add(new Bureau
                {
                    Id = (int)reader["id"],
                    Size = new Size(width, height),
                    Location = new Point(x, y),
                    Orientation = (Orientation)reader["orientation"]
            });
            }
            reader.Close();
            connection.Close();
            return items;
        }
    }
}
