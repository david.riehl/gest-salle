﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestSalle.Dal
{
    enum AppSettings { Host, Username, Password, Port, CharSet, Database}
    public static class AbstractDao
    {
        private static MySqlConnection connection = null;
        public static MySqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    StringBuilder builder = new StringBuilder();
                    foreach (AppSettings appSetting in (AppSettings[])Enum.GetValues(typeof(AppSettings)))
                    {
                        string key = appSetting.ToString();
                        string value = ConfigurationManager.AppSettings.Get(key);
                        builder.Append($"{key}={value};");
                    }
                    connection = new MySqlConnection(builder.ToString());
                }
                return connection;
            }
        }
    }
}
