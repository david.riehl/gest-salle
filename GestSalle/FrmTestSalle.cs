﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestSalle.Classes;

namespace GestSalle
{
    public partial class FrmTestSalle : Form
    {
        public FrmTestSalle()
        {
            InitializeComponent();
        }

        private void FrmTestSalle_Load(object sender, EventArgs e)
        {
            List<Bureau> bureaux = Bureau.GetAll(new Salle() { Id = 1 });
            PlacementBureau(bureaux);
        }

        private void PlacementBureau(List<Bureau> bureaux)
        {
            const int grpSize = 80;
            const int pnlPadding = 1;
            const int grpMargin = 3;

            foreach (Bureau bureau in bureaux)
            {
                int width = bureau.Width * (grpSize + 2 * grpMargin) + 2 * pnlPadding;
                int height = bureau.Height * (grpSize + 2 * grpMargin) + 2 * pnlPadding;
                Panel pnlBureau = new Panel
                {
                    Width = width,
                    Height = height
                };
            }
        }
    }
}
